package com.softdesign.middleandroidtest.data.network;

import com.softdesign.middleandroidtest.data.network.res.CharacterModelRes;
import com.softdesign.middleandroidtest.data.network.res.HouseModelRes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * REST сервис (интерфейс для описания запросов к REST API)
 */
public interface RestService {

    @GET("houses/{houseId}")
    Call<HouseModelRes> getHouse(@Path("houseId") int houseId);

    @GET("characters/{characterId}")
    Call<CharacterModelRes> getCharacter(@Path("characterId") int characterId);

    @GET("characters")
    Call<List<CharacterModelRes>> getCharactersByPage(@Query("page") int page,
                                                      @Query("pageSize") int pageSize);
}
