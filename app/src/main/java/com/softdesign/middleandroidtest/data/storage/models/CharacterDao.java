package com.softdesign.middleandroidtest.data.storage.models;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "CHARACTERS".
*/
public class CharacterDao extends AbstractDao<Character, Long> {

    public static final String TABLENAME = "CHARACTERS";

    /**
     * Properties of entity Character.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Character_id = new Property(1, int.class, "character_id", false, "CHARACTER_ID");
        public final static Property Name = new Property(2, String.class, "name", false, "NAME");
        public final static Property Words = new Property(3, String.class, "words", false, "WORDS");
        public final static Property Born = new Property(4, String.class, "born", false, "BORN");
        public final static Property Died = new Property(5, String.class, "died", false, "DIED");
        public final static Property Titles = new Property(6, String.class, "titles", false, "TITLES");
        public final static Property Aliases = new Property(7, String.class, "aliases", false, "ALIASES");
        public final static Property Father_id = new Property(8, Integer.class, "father_id", false, "FATHER_ID");
        public final static Property Mother_id = new Property(9, Integer.class, "mother_id", false, "MOTHER_ID");
        public final static Property Last_season = new Property(10, String.class, "last_season", false, "LAST_SEASON");
    };

    private DaoSession daoSession;


    public CharacterDao(DaoConfig config) {
        super(config);
    }
    
    public CharacterDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"CHARACTERS\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"CHARACTER_ID\" INTEGER NOT NULL UNIQUE ," + // 1: character_id
                "\"NAME\" TEXT," + // 2: name
                "\"WORDS\" TEXT," + // 3: words
                "\"BORN\" TEXT," + // 4: born
                "\"DIED\" TEXT," + // 5: died
                "\"TITLES\" TEXT," + // 6: titles
                "\"ALIASES\" TEXT," + // 7: aliases
                "\"FATHER_ID\" INTEGER," + // 8: father_id
                "\"MOTHER_ID\" INTEGER," + // 9: mother_id
                "\"LAST_SEASON\" TEXT);"); // 10: last_season
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"CHARACTERS\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Character entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getCharacter_id());
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(3, name);
        }
 
        String words = entity.getWords();
        if (words != null) {
            stmt.bindString(4, words);
        }
 
        String born = entity.getBorn();
        if (born != null) {
            stmt.bindString(5, born);
        }
 
        String died = entity.getDied();
        if (died != null) {
            stmt.bindString(6, died);
        }
 
        String titles = entity.getTitles();
        if (titles != null) {
            stmt.bindString(7, titles);
        }
 
        String aliases = entity.getAliases();
        if (aliases != null) {
            stmt.bindString(8, aliases);
        }
 
        Integer father_id = entity.getFather_id();
        if (father_id != null) {
            stmt.bindLong(9, father_id);
        }
 
        Integer mother_id = entity.getMother_id();
        if (mother_id != null) {
            stmt.bindLong(10, mother_id);
        }
 
        String last_season = entity.getLast_season();
        if (last_season != null) {
            stmt.bindString(11, last_season);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Character entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getCharacter_id());
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(3, name);
        }
 
        String words = entity.getWords();
        if (words != null) {
            stmt.bindString(4, words);
        }
 
        String born = entity.getBorn();
        if (born != null) {
            stmt.bindString(5, born);
        }
 
        String died = entity.getDied();
        if (died != null) {
            stmt.bindString(6, died);
        }
 
        String titles = entity.getTitles();
        if (titles != null) {
            stmt.bindString(7, titles);
        }
 
        String aliases = entity.getAliases();
        if (aliases != null) {
            stmt.bindString(8, aliases);
        }
 
        Integer father_id = entity.getFather_id();
        if (father_id != null) {
            stmt.bindLong(9, father_id);
        }
 
        Integer mother_id = entity.getMother_id();
        if (mother_id != null) {
            stmt.bindLong(10, mother_id);
        }
 
        String last_season = entity.getLast_season();
        if (last_season != null) {
            stmt.bindString(11, last_season);
        }
    }

    @Override
    protected final void attachEntity(Character entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Character readEntity(Cursor cursor, int offset) {
        Character entity = new Character( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getInt(offset + 1), // character_id
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // name
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // words
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // born
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // died
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // titles
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // aliases
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // father_id
            cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9), // mother_id
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10) // last_season
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Character entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setCharacter_id(cursor.getInt(offset + 1));
        entity.setName(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setWords(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setBorn(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setDied(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setTitles(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setAliases(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setFather_id(cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.setMother_id(cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9));
        entity.setLast_season(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Character entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Character entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
