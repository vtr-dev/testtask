package com.softdesign.middleandroidtest.data.managers;

import android.content.Context;
import android.util.Log;

import com.softdesign.middleandroidtest.data.network.RestService;
import com.softdesign.middleandroidtest.data.network.ServiceGenerator;
import com.softdesign.middleandroidtest.data.network.res.CharacterModelRes;
import com.softdesign.middleandroidtest.data.network.res.HouseModelRes;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDao;
import com.softdesign.middleandroidtest.data.storage.models.DaoSession;
import com.softdesign.middleandroidtest.data.storage.models.HouseMember;
import com.softdesign.middleandroidtest.data.storage.models.HouseMemberDao;
import com.softdesign.middleandroidtest.utils.MiddleAndroidTestApplication;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class DataManager {
    private static DataManager INSTANCE = null;

    private Context mContext;

    private PreferencesManager mPreferencesManager;

    private RestService mRestService;

    private DaoSession mDaoSession;

    public DataManager() {
        this.mContext = MiddleAndroidTestApplication.getContext();
        this.mPreferencesManager = new PreferencesManager();
        this.mRestService = ServiceGenerator.createService(RestService.class);
        this.mDaoSession = MiddleAndroidTestApplication.getDaoSession();
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Context getContext() {
        return mContext;
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    //region ============= Network =============

    public Call<HouseModelRes> getHouseModelFromNetwork(int houseId) {
        return mRestService.getHouse(houseId);
    }

    public Call<CharacterModelRes> getCharacterModelFromNetwork(int characterId) {
        return mRestService.getCharacter(characterId);
    }

    public Call<List<CharacterModelRes>> getCharactersByPageFromNetwork(int page, int pageSize) {
        return mRestService.getCharactersByPage(page, pageSize);
    }

    //endregion

    //region ============= Database =============

    public DaoSession getDaoSession() {
        return mDaoSession;
    }


    public List<Character> getCharactersByHouseFromDb(int houseId) {

        List<HouseMember> memberList = mDaoSession.queryBuilder(HouseMember.class)
                .where(HouseMemberDao.Properties.House_id.eq(houseId))
                .build()
                .list();
        List<Integer> membersIds = new ArrayList<>();
        for (HouseMember member : memberList ) {
            membersIds.add(member.getCharacter_id());
        }

        List<Character> characterList = mDaoSession.queryBuilder(Character.class)
                .where(CharacterDao.Properties.Character_id.in(membersIds))
                .build()
                .list();

        Log.d("TAG_DB", characterList.get(0).getName());

        return characterList;
    }

    public Character getCharacterFromDb(int characterId) {
        return mDaoSession.queryBuilder(Character.class)
                .where(CharacterDao.Properties.Character_id.eq(characterId))
                .build()
                .unique();
    }

    //endregion
}
