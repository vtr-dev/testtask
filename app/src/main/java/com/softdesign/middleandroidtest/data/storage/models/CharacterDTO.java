package com.softdesign.middleandroidtest.data.storage.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Character data transfer object
 * http://www.parcelabler.com
 */
public class CharacterDTO implements Parcelable {

    private int mHouseNum;
    private String mName;
    private String mBorn;
    private String mDied;
    private String mTitles;
    private String mAliases;
    private int mFatherId;
    private int mMotherId;
    private String mLastSeason;

    public CharacterDTO(int houseNum, Character characterData) {
        mHouseNum = houseNum;
        mName = characterData.getName();
        mBorn = characterData.getBorn();
        mDied = characterData.getDied();
        mTitles = characterData.getTitles();
        mAliases = characterData.getAliases();
        mFatherId = characterData.getFather_id();
        mMotherId = characterData.getMother_id();
        mLastSeason = characterData.getLast_season();
    }

    protected CharacterDTO(Parcel in) {
        mHouseNum = in.readInt();
        mName = in.readString();
        mBorn = in.readString();
        mDied = in.readString();
        mTitles = in.readString();
        mAliases = in.readString();
        mFatherId = in.readInt();
        mMotherId = in.readInt();
        mLastSeason = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mHouseNum);
        dest.writeString(mName);
        dest.writeString(mBorn);
        dest.writeString(mDied);
        dest.writeString(mTitles);
        dest.writeString(mAliases);
        dest.writeInt(mFatherId);
        dest.writeInt(mMotherId);
        dest.writeString(mLastSeason);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CharacterDTO> CREATOR = new Parcelable.Creator<CharacterDTO>() {
        @Override
        public CharacterDTO createFromParcel(Parcel in) {
            return new CharacterDTO(in);
        }

        @Override
        public CharacterDTO[] newArray(int size) {
            return new CharacterDTO[size];
        }
    };

    public int getHouseNum() {
        return mHouseNum;
    }

    public String getName() {
        return mName;
    }

    public String getBorn() {
        return mBorn;
    }

    public String getDied() {
        return mDied;
    }

    public String getTitles() {
        return mTitles;
    }

    public String getAliases() {
        return mAliases;
    }

    public int getFatherId() {
        return mFatherId;
    }

    public int getMotherId() {
        return mMotherId;
    }

    public String getLastSeason() {
        return mLastSeason;
    }
}
