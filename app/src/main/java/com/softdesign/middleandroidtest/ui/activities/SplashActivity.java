package com.softdesign.middleandroidtest.ui.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.managers.DataManager;
import com.softdesign.middleandroidtest.data.network.res.CharacterModelRes;
import com.softdesign.middleandroidtest.data.network.res.HouseModelRes;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDao;
import com.softdesign.middleandroidtest.data.storage.models.HouseMember;
import com.softdesign.middleandroidtest.data.storage.models.HouseMemberDao;
import com.softdesign.middleandroidtest.utils.AppConfig;
import com.softdesign.middleandroidtest.utils.ConstantManager;
import com.softdesign.middleandroidtest.utils.StrUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    private DataManager mDataManager;
    private CharacterDao mCharacterDao;
    private HouseMemberDao mHouseMemberDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mDataManager = DataManager.getInstance();
        mCharacterDao = mDataManager.getDaoSession().getCharacterDao();
        mHouseMemberDao = mDataManager.getDaoSession().getHouseMemberDao();

        splashStartDelay();
    }

    /**
     * Запускает Splash Screen с задержкой при старте приложения.
     * Затем запускает проверку токена авторизации.
     */
    private void splashStartDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkAppData();
            }
        }, AppConfig.SPLASH_DELAY);
    }

    private void checkAppData() {
        if (mDataManager.getPreferencesManager().getDataLoadedFlag()) {
            processAppDataLoaded();
        } else {
            loadAppData();
        }
    }

    private void loadAppData() {
        showProgress();
        //mCharacterDao.deleteAll();
        //mHouseMemberDao.deleteAll();
        loadHouses(0); // вызывает ожидаемую ошибку при изменении конфигурации в процессе загрузки данных
    }

    private void processAppDataLoaded() {
        startActivity(new Intent(SplashActivity.this, CharacterListActivity.class));
    }

    private void loadHouses(final int houseNum) {
        Call<HouseModelRes> call = mDataManager
                .getHouseModelFromNetwork(ConstantManager.HOUSE_IDS[houseNum]);
        call.enqueue(new Callback<HouseModelRes>() {
            @Override
            public void onResponse(Call<HouseModelRes> call, Response<HouseModelRes> response) {
                if (response.code() == 200) {
                    List<HouseMember> allHouseMembers = new ArrayList<>();
                    for (String memberUrl : response.body().getSwornMembers()) {
                        allHouseMembers.add(new HouseMember(
                                Integer.valueOf(StrUtils.getLastPartOfUrl(memberUrl)),
                                ConstantManager.HOUSE_IDS[houseNum]));
                    }
                    mHouseMemberDao.insertOrReplaceInTx(allHouseMembers);
                    if (houseNum < ConstantManager.HOUSE_IDS.length - 1) {
                        loadHouses(houseNum + 1);
                    }  else {
                        loadCharactersUpPage(1, 50);
                    }
                } else {
                    hideProgress();
                    showToast(getString(R.string.msg_data_loading_error));
                    Log.e(TAG, "onResponse: " + String.valueOf(response.errorBody().source()));
                }
            }

            @Override
            public void onFailure(Call<HouseModelRes> call, Throwable t) {
                hideProgress();
                showToast(getString(R.string.msg_network_error));
                Log.e(TAG, "Network error: " + t.getMessage());
            }
        });
    }

    private void loadCharactersUpPage(final int page, final int pageSize) {
        Call<List<CharacterModelRes>> call = mDataManager.getCharactersByPageFromNetwork(page, pageSize);
        call.enqueue(new Callback<List<CharacterModelRes>>() {
            @Override
            public void onResponse(Call<List<CharacterModelRes>> call, Response<List<CharacterModelRes>> response) {
                if (response.code() == 200) {
                    if (!response.body().isEmpty()) {
                        List<Character> allCharacters = new ArrayList<>();
                        for (CharacterModelRes characterModel : response.body()) {
                            allCharacters.add(new Character(characterModel));
                        }
                        mCharacterDao.insertOrReplaceInTx(allCharacters);
                        loadCharactersUpPage(page + 1, pageSize);
                    } else {
                        hideProgress();
                        mDataManager.getPreferencesManager().saveDataLoadedFlag(true);
                        processAppDataLoaded();
                    }
                } else {
                    hideProgress();
                    showToast(getString(R.string.msg_data_loading_error));
                    Log.e(TAG, "onResponse: " + String.valueOf(response.errorBody().source()));
                }
            }

            @Override
            public void onFailure(Call<List<CharacterModelRes>> call, Throwable t) {
                hideProgress();
                showToast(getString(R.string.msg_network_error));
                Log.e(TAG, "Network error: " + t.getMessage());
            }
        });
    }
}
