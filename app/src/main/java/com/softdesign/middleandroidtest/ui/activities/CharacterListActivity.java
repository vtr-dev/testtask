package com.softdesign.middleandroidtest.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.managers.DataManager;
import com.softdesign.middleandroidtest.ui.adapters.HousesPagerAdapter;
import com.softdesign.middleandroidtest.utils.ConstantManager;

public class CharacterListActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private DrawerLayout mNavigationDrawer;
    private NavigationView mNavigationView;
    private CoordinatorLayout mCoordinatorLayout;

    private HousesPagerAdapter mHousesPagerAdapter;

    private ViewPager mViewPager;
    private TabLayout mTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_container);

        setupToolbar();

        // создает адаптер, который будет возвращать фрагмент для каждой
        // из трех секций (вкладок домов) активности
        mHousesPagerAdapter = new HousesPagerAdapter(getSupportFragmentManager());

        // настройка ViewPager
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mHousesPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                updateDrawer(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        // настройка TabLayout
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        setupDrawer();
    }

    /**
     * Выполняет инициализацию панели Toolbar
     */
    private void setupToolbar() {
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mNavigationDrawer.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Выполняет инициализацию выдвижной панели навигационного меню
     */
    private void setupDrawer() {
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            mNavigationDrawer.closeDrawer(GravityCompat.START);
                            switch (item.getItemId()) {
                                case R.id.stark_menu:
                                    mTabLayout.getTabAt(0).select();
                                    return true;
                                case R.id.lannister_menu:
                                    mTabLayout.getTabAt(1).select();
                                    return true;
                                case R.id.targaryen_menu:
                                    mTabLayout.getTabAt(2).select();
                                    return true;
                            }
                            return false;
                        }
                    }
            );
        }
    }

    /**
     * Выполняет обновление выдвижной панели
     */
    private void updateDrawer(int houseNum) {
        if (mNavigationView != null) {
            View headerView = mNavigationView.getHeaderView(0);
            ((ImageView)headerView.findViewById(R.id.drawer_avatar))
                    .setImageResource(ConstantManager.HOUSE_ICONS[houseNum]);
            ((TextView)headerView.findViewById(R.id.drawer_title))
                    .setText(String.format(getString(R.string.drawer_title),
                            ConstantManager.HOUSE_NAMES[houseNum]));
            mNavigationView.setCheckedItem(ConstantManager.HOUSE_MENU_ITEMS[houseNum]);
        }
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawer != null && mNavigationDrawer.isDrawerOpen(GravityCompat.START)) {
            mNavigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Отображает панель Shackbar, содержащую текстовое сообщение
     * @param message текст сообщения
     */
    private void showShackbar(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }


}
