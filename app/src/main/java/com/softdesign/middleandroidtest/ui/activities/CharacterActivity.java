package com.softdesign.middleandroidtest.ui.activities;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.managers.DataManager;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDTO;
import com.softdesign.middleandroidtest.utils.ConstantManager;

public class CharacterActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private CoordinatorLayout mCoordinatorLayout;

    private ImageView mHouseImage;
    private TextView mWordsTextView, mBornTextView, mDiedTextView, mTitlesTextView, mAliasesTextView;
    private Button mFatherButton, mMotherButton;
    private TableRow mFatherRow, mMotherRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        mHouseImage = (ImageView) findViewById(R.id.house_img);
        mWordsTextView = (TextView) findViewById(R.id.words_tv);
        mBornTextView = (TextView) findViewById(R.id.born_tv);
        mDiedTextView = (TextView) findViewById(R.id.died_tv);
        mTitlesTextView = (TextView) findViewById(R.id.titles_tv);
        mAliasesTextView = (TextView) findViewById(R.id.aliases_tv);
        mFatherButton = (Button) findViewById(R.id.father_btn);
        mMotherButton = (Button) findViewById(R.id.mother_btn);
        mFatherRow = (TableRow) findViewById(R.id.father_row);
        mMotherRow = (TableRow) findViewById(R.id.mother_row);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_container);

        setupToolbar();
        initCharacterData();
    }

    /**
     * Инициализирует Toolbar
     */
    private void setupToolbar() {
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Инициализирует профиль персонажа данными, полученными из активности CharacterListActivity
     */
    private void initCharacterData() {
        CharacterDTO characterDTO = getIntent().getParcelableExtra(ConstantManager.PARCELABLE_KEY);
        final int houseNum = characterDTO.getHouseNum();

        mHouseImage.setImageResource(ConstantManager.HOUSE_POSTERS[houseNum]);
        mCollapsingToolbarLayout.setTitle(characterDTO.getName());

        mWordsTextView.setText(ConstantManager.HOUSE_WORDS[houseNum]);
        mBornTextView.setText(characterDTO.getBorn());
        mDiedTextView.setText(characterDTO.getDied());
        mTitlesTextView.setText(characterDTO.getTitles());
        mAliasesTextView.setText(characterDTO.getAliases());

        if (!characterDTO.getDied().isEmpty()) {
            String message;
            if (!characterDTO.getLastSeason().isEmpty()) {
                message = String.format(
                        getString(R.string.msg_character_died_in_season), characterDTO.getLastSeason());
            } else {
                message = getString(R.string.msg_character_died);
            }
            showSnackbar(message);
        }

        mFatherRow.setVisibility(View.GONE);
        int fatherId = characterDTO.getFatherId();
        if (fatherId > 0) {
            final Character father = DataManager.getInstance().getCharacterFromDb(fatherId);
            if (father != null) {
                mFatherRow.setVisibility(View.VISIBLE);
                mFatherButton.setText(father.getName());
                mFatherButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CharacterDTO characterDTO = new CharacterDTO(houseNum, father);
                        Intent profileIntent = new Intent(CharacterActivity.this, CharacterActivity.class);
                        profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, characterDTO);
                        startActivity(profileIntent);
                    }
                });
            }
        }

        mMotherRow.setVisibility(View.GONE);
        int motherId = characterDTO.getMotherId();
        if (motherId > 0) {
            final Character mother = DataManager.getInstance().getCharacterFromDb(motherId);
            if (mother != null) {
                mMotherRow.setVisibility(View.VISIBLE);
                mMotherButton.setText(mother.getName());
                mMotherButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CharacterDTO characterDTO = new CharacterDTO(houseNum, mother);
                        Intent profileIntent = new Intent(CharacterActivity.this, CharacterActivity.class);
                        profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, characterDTO);
                        startActivity(profileIntent);
                    }
                });
            }
        }

    }

    private void showSnackbar(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }
}
