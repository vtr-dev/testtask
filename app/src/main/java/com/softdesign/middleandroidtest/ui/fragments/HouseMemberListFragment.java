package com.softdesign.middleandroidtest.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.managers.DataManager;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDTO;
import com.softdesign.middleandroidtest.ui.activities.CharacterActivity;
import com.softdesign.middleandroidtest.ui.adapters.CharactersAdapter;
import com.softdesign.middleandroidtest.ui.views.SimpleDividerItemDecoration;
import com.softdesign.middleandroidtest.utils.ConstantManager;

import java.util.List;

/**
 * Фрагмент для отображения списка персонажей, относящихся к дому
 */
public class HouseMemberListFragment extends Fragment {

    private static final String ARG_POSITION = "POSITION";
    private static final String ARG_HOUSE_ID = "HOUSE_ID";

    private RecyclerView mCharactersRecyclerView;

    private List<Character> mHouseMembers;

    public HouseMemberListFragment() {
    }

    /**
     * Возвращает экземпляр фрагмента для дома с заданным id
     */
    public static HouseMemberListFragment newInstance(int position, int houseId) {
        HouseMemberListFragment fragment = new HouseMemberListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        args.putInt(ARG_HOUSE_ID, houseId);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_house_member_list, container, false);

        mCharactersRecyclerView = (RecyclerView) rootView
                .findViewById(R.id.characters_recycler_view);
        mCharactersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCharactersRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        CharactersAdapter charactersAdapter = new CharactersAdapter(
                mHouseMembers,
                getArguments().getInt(ARG_POSITION),
                new CharactersAdapter.CharacterViewHolder.CustomClickListener() {
                    @Override
                    public void onCharacterItemClickListener(int position) {
                        CharacterDTO characterDTO = new CharacterDTO(
                                getArguments().getInt(ARG_POSITION), mHouseMembers.get(position));
                        Intent profileIntent = new Intent(getActivity(), CharacterActivity.class);
                        profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, characterDTO);
                        startActivity(profileIntent);
                    }
                }
        );
        mCharactersRecyclerView.setAdapter(charactersAdapter);

        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        // получение из БД списка персонажей, относящихся к дому
        mHouseMembers = DataManager.getInstance()
                .getCharactersByHouseFromDb(getArguments().getInt(ARG_HOUSE_ID));
    }

}
