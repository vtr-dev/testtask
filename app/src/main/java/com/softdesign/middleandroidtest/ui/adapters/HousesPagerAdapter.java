package com.softdesign.middleandroidtest.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.softdesign.middleandroidtest.ui.fragments.HouseMemberListFragment;
import com.softdesign.middleandroidtest.utils.ConstantManager;

public class HousesPagerAdapter extends FragmentPagerAdapter {

    public HousesPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // Возвратить фрагмент, соответствующий заданной странице
        return HouseMemberListFragment.newInstance(position, ConstantManager.HOUSE_IDS[position]);
    }

    @Override
    public int getCount() {
        // Показывать три страницы.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ConstantManager.HOUSE_NAMES[0].toUpperCase();
            case 1:
                return ConstantManager.HOUSE_NAMES[1].toUpperCase();
            case 2:
                return ConstantManager.HOUSE_NAMES[2].toUpperCase();
        }
        return null;
    }
}
