package com.softdesign.middleandroidtest.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.utils.ConstantManager;

import java.util.List;

public class CharactersAdapter
        extends RecyclerView.Adapter<CharactersAdapter.CharacterViewHolder> {

    private Context mContext;
    private List<Character> mCharacters;
    private int mHouseNum;
    private CharacterViewHolder.CustomClickListener mCustomClickListener;

    public CharactersAdapter(List<Character> characters, int houseNum,
                             CharacterViewHolder.CustomClickListener customClickListener) {
        mCharacters = characters;
        mHouseNum = houseNum;
        mCustomClickListener = customClickListener;
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View convertView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_character_list, parent, false);

        return new CharacterViewHolder(convertView, mCustomClickListener);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        Character character = mCharacters.get(position);

        holder.mAvatarImageView.setImageResource(ConstantManager.HOUSE_ICONS[mHouseNum]);
        holder.mNameTextView.setText(character.getName());
        holder.mTitleTextView.setText(character.getTitles().split("\n")[0]);
        //holder.mTitleTextView.setText(character.getTitles().replace("\n", ", "));
    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }

    public static class CharacterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mAvatarImageView;
        private TextView mNameTextView;
        private TextView mTitleTextView;

        private CustomClickListener mClickListener;

        public CharacterViewHolder(View itemView, CustomClickListener customClickListener) {
            super(itemView);
            this.mClickListener = customClickListener;

            mAvatarImageView = (ImageView) itemView.findViewById(R.id.item_avatar);
            mNameTextView = (TextView) itemView.findViewById(R.id.item_name);
            mTitleTextView = (TextView) itemView.findViewById(R.id.item_title);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onCharacterItemClickListener(getAdapterPosition());
            }
        }

        public interface CustomClickListener {
            void onCharacterItemClickListener(int position);
        }
    }

}
