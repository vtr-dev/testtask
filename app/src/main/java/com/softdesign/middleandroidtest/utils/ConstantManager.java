package com.softdesign.middleandroidtest.utils;

import com.softdesign.middleandroidtest.R;

public interface ConstantManager {

    String TAG_PREFIX = "MAT ";

    String HOUSE_NAME_STARK = "Stark";
    String HOUSE_NAME_LANNISTER = "Lannister";
    String HOUSE_NAME_TARGARYEN = "Targaryen";

    String[] HOUSE_NAMES = {
            HOUSE_NAME_STARK,
            HOUSE_NAME_LANNISTER,
            HOUSE_NAME_TARGARYEN
    };

    String HOUSE_WORDS_STARK = "Winter is Coming";
    String HOUSE_WORDS_LANNISTER = "Hear Me Roar!";
    String HOUSE_WORDS_TARGARYEN = "Fire and Blood";

    String[] HOUSE_WORDS = {
            HOUSE_WORDS_STARK,
            HOUSE_WORDS_LANNISTER,
            HOUSE_WORDS_TARGARYEN
    };

    int HOUSE_ID_STARK = 362;
    int HOUSE_ID_LANNISTER = 229;
    int HOUSE_ID_TARGARYEN = 378;

    int[] HOUSE_IDS = {
            HOUSE_ID_STARK,
            HOUSE_ID_LANNISTER,
            HOUSE_ID_TARGARYEN
    };

    int HOUSE_POSTER_STARK = R.drawable.stark;
    int HOUSE_POSTER_LANNISTER = R.drawable.lannister;
    int HOUSE_POSTER_TARGARYEN = R.drawable.targarien;

    int[] HOUSE_POSTERS = {
            HOUSE_POSTER_STARK,
            HOUSE_POSTER_LANNISTER,
            HOUSE_POSTER_TARGARYEN
    };

    int HOUSE_ICON_STARK = R.drawable.stark_icon_120;
    int HOUSE_ICON_LANNISTER = R.drawable.lanister_icon_120;
    int HOUSE_ICON_TARGARYEN = R.drawable.targarien_icon_120;

    int[] HOUSE_ICONS = {
            HOUSE_ICON_STARK,
            HOUSE_ICON_LANNISTER,
            HOUSE_ICON_TARGARYEN
    };

    int HOUSE_MENU_STARK = R.id.stark_menu;
    int HOUSE_MENU_LANNISTER = R.id.lannister_menu;
    int HOUSE_MENU_TARGARYEN = R.id.targaryen_menu;

    int[] HOUSE_MENU_ITEMS = {
            HOUSE_MENU_STARK,
            HOUSE_MENU_LANNISTER,
            HOUSE_MENU_TARGARYEN
    };

    String IS_DATA_LOADED_KEY = "IS_DATA_LOADED_KEY";

    String PARCELABLE_KEY = "PARCELABLE_KEY";
}
