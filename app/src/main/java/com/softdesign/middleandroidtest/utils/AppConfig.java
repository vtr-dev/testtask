package com.softdesign.middleandroidtest.utils;

/**
 * Конфигурация приложения
 */
public interface AppConfig {
    String BASE_URL = "http://anapioficeandfire.com/api/";
    int MAX_CONNECT_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
    int SPLASH_DELAY = 3000;
}
